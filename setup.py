from globalPack import dataBase, massage

db = dataBase.DataBase()

sql = """
CREATE TABLE 'users' ('id' INTEGER PRIMARY KEY AUTOINCREMENT,
                                    'name' TEXT UNIQUE NOT NULL,
                                    'email' TEXT UNIQUE NOT NULL,
                                    'password' TEXT NOT NULL,
                                    'active' INTEGER DEFAULT '1');



CREATE TABLE 'tokens' ('id' INTEGER PRIMARY KEY AUTOINCREMENT,
                                    'owner' INTEGER NOT NULL,
                                    'token' 'CHAR (64)' UNIQUE NOT NULL,
                                    'deleted' INTEGER DEFAULT '0');


CREATE TABLE 'files' ('id' INTEGER PRIMARY KEY AUTOINCREMENT,
                                    'owner' INTEGER NOT NULL,
                                    'id_file' TEXT UNIQUE NOT NULL,
                                    'name' TEXT NOT NULL,
                                    'type' TEXT DEFAULT NULL,
                                    'size' TEXT NOT NULL,
                                    'upload_time' TEXT NOT NULL,
                                    'updated' INTEGER DEFAULT '0',
                                    'parent' INTEGER DEFAULT '0',
                                    'folder' INTEGER DEFAULT '0',
                                    'hash' TEXT NOT NULL,
                                    'deleted' TEXT DEFAULT '0'  
	                                );
	                                
"""

db.executescript(sql)
from globalPack import massage, config, functions
import time, os, random
from webServer import db
from werkzeug.utils import secure_filename

# TODO Добавить безопасность на имя файла!


def checkToken(token):
    return db.execute("SELECT owner FROM 'tokens' WHERE token = ? AND deleted = 0", token)


def uploadFile(userId, file):
    syncTime = str(time.time())
    fileName = str(file.filename)
    idFile = functions.getHash256(userId + syncTime + fileName + str(random.random()))
    file.save(os.path.join(config.HUBDIR, idFile))
    hashSum = functions.getCheckSum256File(idFile)
    size = str(os.path.getsize(os.path.join(config.HUBDIR, idFile)))
    if len(fileName.split(".")) > 1:
        type = fileName.split(".")[-1]
    else:
        type = ''
    return {"updateTime": syncTime,
            "fileName": fileName,
            "hashSum": hashSum,
            "idFile": idFile,
            "size": size,
            "type": type}

def removeFileFromHub(fileId):
    os.remove(os.path.join(config.HUBDIR, fileId))


from webServer.api import common
from globalPack import functions, config, massage
from webServer import db
import time

def deleteFile(token, fileId):
    if token != '':
        userId = common.checkToken(token)
        if userId != []:
            userId = str(userId[0][0])
            db.execute("UPDATE 'files' SET deleted=? WHERE id_file=? and owner=?",
                       str(time.time()), fileId, userId)
            return massage.MSG_API_DELETED_FILE
        else:
            return massage.MSG_INCORRECT_DATA
    else:
        return massage.MSG_INCORRECT_DATA
from webServer.api import common
from globalPack import functions, config, massage
from flask import send_file
from webServer import db

def checkValidToken(token):
    if token != '':
        userId = common.checkToken(token)
        if userId != []:
            return massage.constructorJSONMassage(config.CODE_VALID, "True")
        else:
            return massage.constructorJSONMassage(config.CODE_NOT_VALID, "False")
    else:
        return str(False)
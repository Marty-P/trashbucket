from webServer.api import common
from globalPack import functions, config, massage
from flask import send_file
from webServer import db

def getFile(token, file_id):
    if token != '':
        userId = common.checkToken(token)
        fileName = db.execute("SELECT name FROM 'files' WHERE owner = ? AND id_file = ? AND NOT deleted = 1",
                              str(userId[0][0]), file_id)
        if fileName != []:
            return send_file(config.HUBDIR + '\\' + file_id, file_id,
                             as_attachment=True, attachment_filename=str(fileName[0][0]))
        else:
            return massage.MSG_INCORRECT_DATA
    else:
        return massage.MSG_INCORRECT_DATA
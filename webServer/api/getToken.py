from globalPack import functions, config, massage
from webServer import db
import time

def getToken(name, password):
    if (name != '') and (password != ''):
        password =  functions.getHash256(str(password))
        userId = db.execute("SELECT id FROM 'users' WHERE name = ? AND password = ?", str(name), password)
        if userId != []:
            userId = userId[0][0]
            amtTokens = int(db.execute("SELECT COUNT(*) FROM 'tokens' WHERE owner=?", str(userId))[0][0])
            if amtTokens < config.MAX_AMT_TOKENS:
                token = functions.getHash256(str(userId) + str(time.time()))
                db.execute("INSERT INTO 'tokens' (owner, token) VALUES (?,?)", userId, token)
                return massage.constructorJSONMassage(config.CODE_SUCCESS, token)
            else:
                return massage.MSG_API_MAX_AMT_TOKENS
        else:
            return massage.MSG_INCORRECT_DATA
    else:
        return massage.MSG_INCORRECT_DATA
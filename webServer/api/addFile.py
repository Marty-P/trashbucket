from webServer.api import common

from globalPack import functions, config, massage
from webServer import db

def addFile(token, file):
    if token != '':
        userId = common.checkToken(token)
        if userId != []:
            userId = str(userId[0][0])
            sevedFileInfo = common.uploadFile(userId, file)
            idFile = db.execute("SELECT id_file FROM 'files' WHERE owner = ? AND hash = ? AND name = ? AND deleted = 0",
                             userId, sevedFileInfo["hashSum"], sevedFileInfo["fileName"])
            if len(idFile) == 0:
                db.execute("INSERT INTO 'files' (owner, upload_time, name, hash, id_file, size, type) VALUES (?,?,?,?,?,?,?)",
                           userId, sevedFileInfo['updateTime'], sevedFileInfo["fileName"],
                           sevedFileInfo["hashSum"], sevedFileInfo["idFile"],
                           sevedFileInfo["size"], sevedFileInfo["type"])
                return sevedFileInfo["idFile"] + '\n'
            else:
                common.removeFileFromHub(sevedFileInfo["idFile"])
                return idFile[0][0] + '\n'
        else:
            return massage.MSG_INCORRECT_DATA
    else:
        return massage.MSG_INCORRECT_DATA





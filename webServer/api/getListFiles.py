from webServer.api import common
from globalPack import massage, config
from webServer import db
import json

# TODO Розобраться с кирилицей при выводе JSON

def getListFiles(token):
    if token != '':
        userId = common.checkToken(token)
        if userId != []:
            userId = userId[0][0]
            listFile = db.execute("SELECT * FROM 'files' WHERE owner = ? AND deleted = 0 AND updated = 0", userId)
            return massage.constructorJSONMassage(config.CODE_SUCCESS,"", __getList(listFile))
        else:
            return massage.MSG_INCORRECT_DATA
    else:
        return massage.MSG_INCORRECT_DATA

def __getList(files):
    data = {}
    data['Total'] = len(files)
    data['Items'] = []
    for file in files:
        item = {}
        item['Name'] = file[3]
        item['Hash'] = file[10]
        item['UploadTime'] = file[6]
        item['IdFile'] = file[2]
        item['Size'] = file[5]
        item['Type'] = file[4]
        data['Items'].append(item)
    return data

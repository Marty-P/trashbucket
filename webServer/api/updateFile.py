from webServer.api import common
from globalPack import functions, config, massage
from webServer import db
from webServer.api.addFile import addFile

def updateFile(token, idFile, file):
    if token != '':
        userId = common.checkToken(token)
        if userId != []:
            userId = str(userId[0][0])
            sevedFileInfo = common.uploadFile(userId, file)
            dataOldFile = db.execute("SELECT id, name, hash, updated, deleted FROM 'files' WHERE owner=? AND id_file=?",
                                     userId, idFile)

            if dataOldFile != []:
                if str(dataOldFile[0][1]) == sevedFileInfo['fileName']:
                    if dataOldFile[0][4] != 1 and dataOldFile[0][3] == 0:
                        if sevedFileInfo['hashSum'] != dataOldFile[0][2]:
                            db.execute("INSERT INTO 'files' (owner, upload_time, parent, name, hash, id_file, size, type)"
                                       " VALUES (?,?,?,?,?,?,?,?)",
                                       userId, sevedFileInfo['updateTime'], str(dataOldFile[0][0]),
                                       sevedFileInfo["fileName"], sevedFileInfo["hashSum"], sevedFileInfo["idFile"],
                                       sevedFileInfo["size"], sevedFileInfo["type"])
                            dataNewFile = db.execute("SELECT id, name FROM 'files' WHERE owner=? AND id_file=?",
                                                     userId, sevedFileInfo["idFile"])
                            if dataNewFile != []:
                                db.execute("UPDATE 'files' SET updated=? WHERE id_file=? and owner=?",
                               str(dataNewFile[0][0]), idFile, userId)
                                return sevedFileInfo['idFile']
                            else:
                                return massage.MSG_DB_ERROR_SQL_QUERY
                        else:
                            common.removeFileFromHub(sevedFileInfo['idFile'])
                            return idFile
                    else:
                        common.removeFileFromHub(sevedFileInfo['idFile'])
                        return massage.constructorJSONMassage(config.CODE_OLD_FILE, "File is old. Unable to update it.")
                else:
                    return massage.MSG_INCORRECT_DATA
            else:
                return massage.MSG_INCORRECT_DATA
        else:
            return massage.MSG_INCORRECT_DATA
    else:
        return massage.MSG_INCORRECT_DATA
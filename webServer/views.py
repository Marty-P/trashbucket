from flask import render_template
from webServer import models

def index():
    return 'index\n'

def getRegistration():
    return render_template('registration.html')

def postRegistration(name, password, email):
    return models.registration(name, password, email)

def getLogin():
    return render_template('login.html')

def postLogin(name, password, remember):
    return getLogin()

def error404():
    return '404 Page not found\n'
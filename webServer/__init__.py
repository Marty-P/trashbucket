from flask import Flask
from globalPack import dataBase
db = dataBase.DataBase()
webServer = Flask(__name__)
webServer.static_folder = 'templates/style'

from webServer import controllers
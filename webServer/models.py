from webServer import db
from globalPack import massage, functions

def registration(name, password, email):
    password = functions.getHash256(password)
    db.execute("INSERT INTO 'users' (name, password, email) VALUES (?, ?, ?)",
                        str(name).lower(), password, str(email))
    return 'Registered'

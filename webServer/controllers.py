from flask import request, abort
from webServer import webServer
from webServer import views

@webServer.route('/')
@webServer.route('/index')
def index():
    return views.index()


@webServer.route('/registration', methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        return views.postRegistration(request.form['name'], request.form['password'], request.form['email'])
    else:
        return views.getRegistration()


@webServer.route('/login',  methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        return views.postLogin(request.form['name'], request.form['password'], request.form['remember'])
    else:
        return views.getLogin()


#######################################
################  API  ################
#######################################

@webServer.route('/api/<string:method>',  methods=['GET', 'POST'])
def api(method):
    if method.lower() == 'gettoken':
        from webServer.api.getToken import getToken
        return getToken(request.args.get('name', ''), request.args.get('password', ''))
    elif method.lower() == 'addfile':
        from webServer.api.addFile import addFile
        return addFile(request.args.get('token', ''), request.files['file'])
    elif method.lower() == 'getfile':
        from webServer.api.getFile import getFile
        return getFile(request.args.get('token', ''), request.args.get('file_id', ''))
    elif method.lower() == 'getlistfiles':
        from webServer.api.getListFiles import getListFiles
        return getListFiles(request.args.get('token', ''))
    elif method.lower() == "deletefile":
        from webServer.api.deleteFile import deleteFile
        return deleteFile(request.args.get('token', ''), request.args.get('file_id', ''))
    elif method.lower() == "updatefile":
        from webServer.api.updateFile import updateFile
        return updateFile(request.args.get('token', ''), request.args.get('file_id', ''), request.files['file'])
    elif method.lower() == "checkvalidtoken":
        from webServer.api.checkValidToken import checkValidToken
        return checkValidToken(request.args.get('token', ''))
    else:
        abort(404)
from globalPack import config, massage
from flask import abort
import sqlite3


class DataBase:
    def __init__(self):
        self.connection = sqlite3.connect(config.DATABASE)

    def execute(self, query, *args):
        cursor = self.connection.cursor()
        try:
            cursor.execute(query, args)
        except sqlite3.DatabaseError:
            #abort(404)
            return "error"
        else:
            self.connection.commit()
            return cursor.fetchall()

    def executescript(self, script):
        cursor = self.connection.cursor()
        try:
            cursor.executescript(script)
        except sqlite3.DatabaseError:
            #abort(404)
            return "error"
        else:
            self.connection.commit()
            return cursor.fetchall()

    def __del__(self):
        self.connection.close()

import json
from globalPack import config

def constructorJSONMassage(code, text, data=""):
    massage = {}
    massage["Code"] = code
    massage["Text"] = text
    massage["Data"] = data
    return json.dumps(massage)


##################################
######### GLOBAL MASSAGE #########
##################################
MSG_INCORRECT_DATA = constructorJSONMassage(config.CODE_FAILURE, "The transmitted data is incorrect")
MSG_DB_ERROR_SQL_QUERY = constructorJSONMassage(config.CODE_FAILURE, "DataBase: Can not execute requestt")
MSG_API_ERROR_404 = constructorJSONMassage(config.CODE_FAILURE, "API not found")
MSG_API_MAX_AMT_TOKENS = constructorJSONMassage(config.CODE_FAILURE, "Token limit exceeded")
MSG_API_DELETED_FILE = constructorJSONMassage(config.CODE_SUCCESS, "File deleted")
MSG_API_ADDED_FILE = "File added \n"

# TODO Сделать сообщения в формате JSON




import os, hashlib
from globalPack import config


def getCheckSum256File(fileName):
    block_size = 65536
    checkSum = hashlib.sha256()
    with open(os.path.join(config.HUBDIR, fileName), 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            checkSum.update(block)
    return checkSum.hexdigest()

def getHash256(line):
    strline = str(line)
    return hashlib.sha224(strline.encode('utf-8')).hexdigest()

